// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package challenges.domain;

import challenges.domain.Challenge;
import challenges.domain.Goal;
import challenges.domain.MetricGoal;
import challenges.domain.MetricGoalStatus;
import java.util.Date;
import java.util.Set;

privileged aspect Goal_Roo_JavaBean {
    
    public String Goal.getTitle() {
        return this.title;
    }
    
    public void Goal.setTitle(String title) {
        this.title = title;
    }
    
    public String Goal.getDescription() {
        return this.description;
    }
    
    public void Goal.setDescription(String description) {
        this.description = description;
    }
    
    public Date Goal.getDeadline() {
        return this.deadline;
    }
    
    public void Goal.setDeadline(Date deadline) {
        this.deadline = deadline;
    }
    
    public MetricGoal Goal.getMetric() {
        return this.metric;
    }
    
    public void Goal.setMetric(MetricGoal metric) {
        this.metric = metric;
    }
    
    public Set<MetricGoalStatus> Goal.getStatuses() {
        return this.statuses;
    }
    
    public void Goal.setStatuses(Set<MetricGoalStatus> statuses) {
        this.statuses = statuses;
    }
    
    public Challenge Goal.getChallenge() {
        return this.challenge;
    }
    
    public void Goal.setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }
    
}
