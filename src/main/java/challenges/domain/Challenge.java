package challenges.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findChallengesByOwner" })
public class Challenge {

    @NotNull
    @Size(min = 2)
    private String title;

    @NotNull
    @ManyToOne
    private Challenger owner;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Challenger> challengers = new HashSet<Challenger>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Goal> goals = new HashSet<Goal>();

    public static TypedQuery<Challenge> findChallengesByOwner(Challenger owner) {
        if (owner == null) throw new IllegalArgumentException("The owner argument is required");
        EntityManager em = Challenge.entityManager();
        TypedQuery<Challenge> q = em.createQuery("SELECT o FROM Challenge AS o WHERE o.owner = :owner", Challenge.class);
        q.setParameter("owner", owner);
        return q;
    }
}

