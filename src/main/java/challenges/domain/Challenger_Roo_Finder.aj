// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package challenges.domain;

import challenges.domain.Challenger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect Challenger_Roo_Finder {
    
    public static TypedQuery<Challenger> Challenger.findChallengersByEmailEquals(String email) {
        if (email == null || email.length() == 0) throw new IllegalArgumentException("The email argument is required");
        EntityManager em = Challenger.entityManager();
        TypedQuery<Challenger> q = em.createQuery("SELECT o FROM Challenger AS o WHERE o.email = :email", Challenger.class);
        q.setParameter("email", email);
        return q;
    }
    
}
