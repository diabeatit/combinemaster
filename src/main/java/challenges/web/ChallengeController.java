package challenges.web;

import challenges.domain.Challenge;
import challenges.domain.Challenger;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/challenges")
@Controller
@RooWebScaffold(path = "challenges", formBackingObject = Challenge.class)
@RooWebFinder
public class ChallengeController {

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        boolean userIsAdmin = false;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> roles = (List<GrantedAuthority>) auth.getAuthorities();
        for (GrantedAuthority ga : roles) {
            if (ga.getAuthority().equals("ROLE_ADMIN")) {
                userIsAdmin = true;
            }
        }
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            if (userIsAdmin) {
                uiModel.addAttribute("challenges", Challenge.findChallengeEntries(firstResult, sizeNo));
            } else {
                String user = auth.getName();
                Challenger owner = Challenger.findChallengersByEmailEquals(user).getSingleResult();
                uiModel.addAttribute("challenges", Challenge.findChallengesByOwner(owner).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            }
            float nrOfPages = (float) Challenge.countChallenges() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("challenges", Challenge.findAllChallenges());
        }
        return "challenges/list";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Challenge challenge, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, challenge);
            return "challenges/create";
        }
        uiModel.asMap().clear();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName();
        Challenger owner = Challenger.findChallengersByEmailEquals(user).getSingleResult();
        challenge.setOwner(owner);
        challenge.persist();
        return "redirect:/challenges/" + encodeUrlPathSegment(challenge.getId().toString(), httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Challenge challenge, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, challenge);
            return "challenges/update";
        }
        uiModel.asMap().clear();
        challenge.merge();
        return "redirect:/challenges/" + encodeUrlPathSegment(challenge.getId().toString(), httpServletRequest);
    }
}
