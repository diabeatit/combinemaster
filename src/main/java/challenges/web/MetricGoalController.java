package challenges.web;

import challenges.domain.MetricGoal;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/metricgoals")
@Controller
@RooWebScaffold(path = "metricgoals", formBackingObject = MetricGoal.class)
public class MetricGoalController {
}
